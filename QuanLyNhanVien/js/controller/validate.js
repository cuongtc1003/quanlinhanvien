// hợp lệ => return true

// kiểm tra rỗng
function ktrRong(value,span){
    if(value==''||value==0||value=="Chọn chức vụ"){
        document.getElementById(span).innerHTML='Vui lòng điền/chọn dữ liệu'
        return false;
    }else{
        document.getElementById(span).innerHTML=``;
        return true;   
    };    
};

// kiểm tra trùng
function ktrTrung(maNV, DSNV){
    // findIndex trả về vị trí của item nếu điều kiện true, nếu ko tìm thấy trả về -1
    var viTri=DSNV.findIndex(function(item){
        return item.maNV == maNV;
    });
    if (viTri!=-1){
        document.getElementById('tbTKNV').innerHTML=`Mã nhân viên đã tồn tại, vui lòng nhập mã khác.`;
        return false;
    }else{
        document.getElementById('tbTKNV').innerHTML=``;
        return true;
    };
};

// kiểm tra tài khoản 
function ktrTaiKhoan(maNV, span, min, max){
    const re =/^[0-9]+$/;
    var isValid=re.test(maNV);
    console.log(isValid);
    if (isValid==false || maNV.length<min || maNV.length>max){
        document.getElementById(span).innerHTML=`Tài khoản tối đa ${min} - ${max} ký số`;
        return false;
    }else{
        document.getElementById(span).innerHTML=``;
        return true;
    }

};

// kiểm tra tên
function ktrTen(name,span){
    const re =
    /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
    var isValid=re.test(name);
    if (isValid){
        document.getElementById(span).innerHTML=``;
        return true;
    }else{
        document.getElementById(span).innerHTML='Tên nhân viên phải là chữ'
        return false;
    };
};

// kiểm tra email
function ktrEmail(email,span){
    const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;    
    var isValid=re.test(email);
    if (isValid){
        document.getElementById(span).innerHTML=``;
        return true;
    }else{
        document.getElementById(span).innerHTML='Email không đúng định dạng';
        return false;
    };
};

// kiểm tra mật khẩu
function ktrMatKhau(pass,span,min,max){
    const re=/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
    var isValid=re.test(pass);
    if (isValid==false||pass.length<min||pass.length>max){
        document.getElementById(span).innerHTML=`Mật Khẩu từ ${min}-${max} ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)`;
        return false;
    }else{
        document.getElementById(span).innerHTML=``;
        return true;
    };
};

// kiểm tra ngày
function ktrNgay(date,span){
    const re = /^((0[1-9]|[12][0-9]|3[01])(\/)(0[13578]|1[02]))|((0[1-9]|[12][0-9])(\/)(02))|((0[1-9]|[12][0-9]|3[0])(\/)(0[469]|11))(\/)\d{4}$/;
    var isValid=re.test(date);
    if (isValid){
        document.getElementById(span).innerHTML=``;
        return true;
    }else{
        document.getElementById(span).innerHTML='Ngày không hợp lệ';
        return false;
    };
};

// kiểm tra lương
function ktrLuong(luong,span){
    const re =/^[0-9]+$/;
    var isValid=re.test(luong);
    if(isValid==false||luong<1000000||luong>20000000){
        document.getElementById(span).innerHTML='Lương cơ bản 1 000 000 - 20 000 000';
        return false;
    }else{
        document.getElementById(span).innerHTML=``;
        return true;
    };
};

// kiểm tra giờ làm
function ktrGioLam(gio,span,min,max){
    const re =/^[0-9]+$/;
    var isValid=re.test(gio);

    if(isValid==false||gio<min||gio>max){
        document.getElementById(span).innerHTML=`Số giờ làm trong tháng ${min} - ${max} giờ`;
        return false;
    }else{
        document.getElementById(span).innerHTML=``;
        return true;
    };
};

function validate(nv){
    var isValid=true;
    // ktr rỗng
    isValid=
        ktrRong(nv.maNV,'tbTKNV') &
        ktrRong(nv.tenNV,'tbTen') &         
        ktrRong(nv.email,'tbEmail') &         
        ktrRong(nv.pass,'tbMatKhau') &         
        ktrRong(nv.ngayLam,'tbNgay') &         
        ktrRong(nv.luongCB,'tbLuongCB') &         
        ktrRong(nv.role,'tbChucVu') &         
        ktrRong(nv.gioLam,'tbGiolam')         
    ;
    if (isValid){
        // ktra trùng && tài khoản
        isValid=ktrTrung(nv.maNV,DSNV) && ktrTaiKhoan(nv.maNV,'tbTKNV',4,6);
        // ktra tên , email, ngày làm
        isValid=isValid & 
            ktrTen(nv.tenNV,'tbTen') & 
            ktrEmail(nv.email,'tbEmail') & 
            ktrMatKhau(nv.pass,'tbMatKhau',6,10) & 
            ktrNgay(nv.ngayLam,'tbNgay') &
            ktrLuong(nv.luongCB,'tbLuongCB') &
            ktrGioLam(nv.gioLam,'tbGiolam',80,200);
    };    
    return isValid;
}
function validateCapNhat(nv){
    var isValid=true;
    // ktr rỗng
    isValid=        
        ktrRong(nv.tenNV,'tbTen') &         
        ktrRong(nv.email,'tbEmail') &         
        ktrRong(nv.pass,'tbMatKhau') &         
        ktrRong(nv.ngayLam,'tbNgay') &         
        ktrRong(nv.luongCB,'tbLuongCB') &         
        ktrRong(nv.role,'tbChucVu') &         
        ktrRong(nv.gioLam,'tbGiolam')         
    ;
    if (isValid){        
        // ktra tên , email, ngày làm
        isValid=ktrTen(nv.tenNV,'tbTen') & 
            ktrEmail(nv.email,'tbEmail') & 
            ktrMatKhau(nv.pass,'tbMatKhau',6,10) & 
            ktrNgay(nv.ngayLam,'tbNgay') &
            ktrLuong(nv.luongCB,'tbLuongCB') &
            ktrGioLam(nv.gioLam,'tbGiolam',80,200);
    };    
    return isValid;
}
