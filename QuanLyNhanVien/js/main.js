var DSNV=[];
// lấy dữ liệu từ Local Storage khi load lại trang
let dataJson=localStorage.getItem('DSNV_LOCAL');
if (dataJson!=null){
        // covert dữ lieuj json trở lại thành chuỗi vào bỏ vào DSSV
    DSNV=JSON.parse(dataJson);
    // render DSSV ra màn hình
    render();
};
function resetbutton(){
    reset();
    document.getElementById("btnThemNV").disabled = false;
    document.getElementById("btnCapNhat").disabled = true;
}
function themNV(){
    // lấy thông tin từ form
    var nv=layThongTinTuForm();
    // validate ~ kiểm tra dữ liệu
    var isValid = true;
    isValid=validate(nv);
    if(isValid){        
        // push vào DSNV
        DSNV.push(nv);
        // lưu mảng vào local storage
        localStorageDuLieu();
        // render ra HTML
        render();
        // trả về form rỗng
        reset();
    };
};

function xoaNV(maNV){
    var viTri=timKiemViTri(maNV);
    if (viTri!=-1){
        DSNV.splice(viTri,1);
    };
    // lưu mảng vào local storage
    localStorageDuLieu();
    // render DSSV MỚI ra màn hình
    render();
};

function capNhat(){    
    var id=document.getElementById('tknv').value;
    var viTri=timKiemViTri(id);
    if(viTri!=1){
        // lấy thông tin từ form
        var nv=layThongTinTuForm();
        // validate ~ kiểm tra dữ liệu
        var isValid = true;
        isValid=validateCapNhat(nv);
        if(isValid){
            DSNV[viTri]=layThongTinTuForm();
            // trả về form rỗng
            reset();
            // lưu mảng vào local storage
            localStorageDuLieu();
            // render ra HTML
            render(); 
            };            
    };    
};

function reset(){
    // reset ô input
    document.getElementById('tknv').value="";
    document.getElementById("tknv").disabled = false;
    document.getElementById('name').value="";
    document.getElementById('email').value="";
    document.getElementById('password').value="";
    document.getElementById('datepicker').value="";
    document.getElementById('luongCB').value="";
    document.getElementById('chucvu').value="";
    document.getElementById('gioLam').value="";    

    // reset span
    document.getElementById('tbTKNV').innerHTML=``;
    document.getElementById('tbTen').innerHTML=``;
    document.getElementById('tbEmail').innerHTML=``;
    document.getElementById('tbMatKhau').innerHTML=``;
    document.getElementById('tbNgay').innerHTML=``;
    document.getElementById('tbLuongCB').innerHTML=``;
    document.getElementById('tbChucVu').innerHTML=``;
    document.getElementById('tbGiolam').innerHTML=``;
};

function suaNV(maNV){
    document.getElementById("btnCapNhat").disabled = false;
    document.getElementById("btnThemNV").disabled = true;
    var viTri=timKiemViTri(maNV);
    if (viTri!=-1){
        document.getElementById('tknv').value=DSNV[viTri].maNV;
        document.getElementById("tknv").disabled = true;
        document.getElementById('name').value=DSNV[viTri].tenNV;
        document.getElementById('email').value=DSNV[viTri].email;
        document.getElementById('password').value=DSNV[viTri].pass;
        document.getElementById('datepicker').value=DSNV[viTri].ngayLam;
        document.getElementById('luongCB').value=DSNV[viTri].luongCB;
        document.getElementById('chucvu').value=DSNV[viTri].role;
        document.getElementById('gioLam').value=DSNV[viTri].gioLam;
    };
};
// lấy thẻ input
var input = document.getElementById("searchName");
// định nghĩa hàm xử lý myFunction
function myFunction() {
    var filter, table, tr, td, i;
     // lấy giá trị người dùng nhập
    filter = input.value.toUpperCase();

    // lấy phần bảng hiển thị kết quả
    table = document.getElementById("myTable");
    // lấy tất cả các thẻ tr
    tr = table.getElementsByTagName("tr");

    //Nếu filter không có giá trị ẩn các kết quả
    if (filter){
        // lặp qua tất cả các thẻ tr
        for (i = 0; i < tr.length; i++) {
        // lấy giá trị của thẻ td đầu tiên đại diện cho tên club
            td = tr[i].getElementsByTagName("td")[6];
            if (td) {
                 // kiểm tra giá trị filter có tồn tại trong thẻ tr không
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    //nếu có hiển thị chúng
                    table.style.display = "table";
                    tr[i].style.display = "";
                } else {
                    // nếu không ẩn chúng
                    tr[i].style.display = "none";
                }
            }       
            }
    }else{
        render();
    }
}
//gán sự kiện cho thẻ input
input.addEventListener("keyup", myFunction);
