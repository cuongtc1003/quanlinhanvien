function NhanVien(
    _maNV,
    _tenNV,
    _email,
    _pass,
    _ngayLam,
    _luongCB,
    _role,
    _gioLam){
        this.maNV= _maNV;
        this.tenNV= _tenNV;
        this.email= _email;
        this.pass= _pass;
        this.ngayLam= _ngayLam;
        this.luongCB= _luongCB;
        this.role= _role;
        this.gioLam= _gioLam;
        this.tongLuong= function(){
            if (this.role=='Sếp'){
                return (this.luongCB*3);
            } else if (this.role=='Trưởng phòng'){
                return (this.luongCB*2);
            } else{
                return (this.luongCB*1);
            }
        };
        this.xepLoai= function(){
            if(this.gioLam>=192){
                return 'Nhân viên xuất sắc';
            } else if(this.gioLam>=176){
                return 'Nhân viên giỏi';
            } else if(this.gioLam>=160){
                return 'Nhân viên khá';
            } else {
                return 'Nhân viên trung bình';
            }
        };
};
